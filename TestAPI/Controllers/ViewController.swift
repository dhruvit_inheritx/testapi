//
//  ViewController.swift
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var dictUserDetail = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        dictUserDetail = self.fetchUserDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    var managedObjectContext : NSManagedObjectContext = {
//        return APPDELEGATE.persistentContainer.viewContext
//    }()
    
    func save(dictData : [String:Any]) {
        let context: NSManagedObjectContext! = APPDELEGATE.persistentContainer.viewContext
        // Create a new managed object
        let newUser: NSManagedObject = NSEntityDescription.insertNewObject(forEntityName: KUSERDETAILS, into: context!)
        
        for (key,value) in dictData as! [String:String] {
            newUser.setValue(value, forKey: key)
        }
        
        // Save the object to persistent store
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func fetchUserDetails() -> [String:Any] {
        //1
        let managedContext =
            APPDELEGATE.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: KUSERDETAILS)
        
        var dictDetail = [String:Any]()
        //3
        do {
            let data = try managedContext.fetch(fetchRequest) as [NSManagedObject]
            
            let arrKeys = Array(data[0].entity.attributesByName.keys)
            dictDetail = data[0].dictionaryWithValues(forKeys: arrKeys)
//            dictDetail = dictionaryWithValues(forKeys: arrKeys)
            print("dictDetail :- ",dictDetail)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return dictDetail
    }
    
    @IBAction func callWebService() {
        let dictParam : [String : Any] = [KEMAIL:"dhruvit@inheritx.com",KDEVICETOKEN:"0",KFBID:"227299517772920"]
        APIManager.sharedInstance.callWebServiceWithParameter(parameters: dictParam, urlPath: ApiCallPath.login.rawValue, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (apiResult, response) in
            
            if apiResult == ApiResult.success {
                let dict = response as! [String:Any]
                print("dict :- ",dict)
                
                self.save(dictData: dict[KUSERDETAILS_RESPONSE] as! [String : Any])
                
            }
            else {
                Helper.showAlertBar(alertBarType: AlertBarType.Error, message: response as! String)
            }
        }
    }
    
//    @IBAction func callWebService() {
//
//        let strUrl = "http://inheritxdev.net/PerfectAttire_dev/code/web/api/v1/api/login"
//        let apiUrl = URL(string: strUrl)
//
//        let dictParam : [String : Any] = [KEMAIL:"dhruvit@inheritx.com",KDEVICETOKEN:"0",KFBID:"227299517772920"]
//
//        let jsonData = try? JSONSerialization.data(withJSONObject: dictParam,
//                                                   options: .prettyPrinted)
//
//        var request = URLRequest(url: apiUrl!)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.httpBody = jsonData
//
//        let configuration = URLSessionConfiguration.default
//        let session = URLSession(configuration: configuration)
//
//        session.dataTask(with: request) { (data, response, error) in
//
//            if error == nil {
//
//                if data != nil {
//
//                    let dictData = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    print("dictData:- ",dictData!)
//                }
//                else {
//                    print("No Data Found.")
//                }
//            }
//            else {
//                print("error :- ",error?.localizedDescription as Any)
//            }
//
//        }.resume()
//    }
    
}
