//
//  CustomAlert.m
//  CustomAlert
//
//  Created by Mariya Kholod on 4/23/13.
//  Copyright (c) 2013 Mariya Kholod. All rights reserved.
//

#import "CustomAlert.h"
//#import "ByBuy-Bridging-Header.h"

#define MAX_ALERT_HEIGHT 300.0
#define ALERT_BTN_HEIGHT 25.0
#define ALERT_BTN_WIDTH 85.0

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//let window: UIWindow = UIApplication.sharedApplication().keyWindow!
@implementation CustomAlert

#define window [UIApplication sharedApplication].keyWindow

@synthesize delegate;


- (id)initWithTitle:(NSString *)title message:(id)message delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock1)completion
{
    self.completionBock1 = completion;
    
    float screenHeight;
    float screenWidth;
    
    screenHeight = window.frame.size.height;
    screenWidth = window.frame.size.width;
    
    CGRect frame;
    
    frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft || [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight)
    {
        if(!IS_OS_8_OR_LATER)
        {
            frame = CGRectMake(0.0, 0.0, screenHeight, screenWidth);
        }
    }
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = AlertDelegate;
        self.alpha = 1.0;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        float alert_width = 300;
        float alert_height = 5.0;
        
        //add text
        UILabel *TitleLbl;
        UIScrollView *MsgScrollView;
        UIView *buttonBaseView = [[UIView alloc] init];
        if (title)
        {
            TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, 30.0)];
            TitleLbl.adjustsFontSizeToFitWidth = YES;
            TitleLbl.font = [UIFont fontWithName:@"GothamBold" size:18];
            TitleLbl.textAlignment = NSTextAlignmentCenter;
            //             lblHeader.font = UIFont(name: colaborateMedium, size: 17)
            //            TitleLbl.minimumFontSize = 12.0;
            [TitleLbl setMinimumScaleFactor:12.0/[UIFont labelFontSize]];
            
            TitleLbl.backgroundColor = [UIColor clearColor];
            TitleLbl.textColor = alertColor;
            TitleLbl.text = title;
            
            alert_height += TitleLbl.frame.size.height + 5.0;
        }
        else
            alert_height += 15.0;
        
        if (message)
        {
            
            float max_msg_height = MAX_ALERT_HEIGHT - alert_height - ((cancelButtonTitle || otherButtonTitle)?(ALERT_BTN_HEIGHT+30.0):30.0);
            
            UILabel *MessageLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 0.0, alert_width-30.0, 0.0)];
            MessageLbl.numberOfLines = 0;
            MessageLbl.font = [UIFont fontWithName:@"GothamMedium" size:15];
            MessageLbl.textAlignment = NSTextAlignmentCenter;
            MessageLbl.backgroundColor = [UIColor clearColor];
            MessageLbl.textColor = [UIColor colorWithRed:84.0/255.0 green:84.0/255 blue:84.0/255 alpha:1.0];

            if ([message isKindOfClass:[NSMutableAttributedString class]]) {
                MessageLbl.attributedText = message;
            }
            else{
                MessageLbl.text = message;
            }
            
            
            [MessageLbl sizeToFit];
            MessageLbl.frame = CGRectMake(5.0, 0.0, alert_width-30.0, MessageLbl.frame.size.height);
            
            while (MessageLbl.frame.size.height>max_msg_height && MessageLbl.font.pointSize>12) {
                MessageLbl.font = [UIFont systemFontOfSize:MessageLbl.font.pointSize-1];
                [MessageLbl sizeToFit];
                MessageLbl.frame = CGRectMake(5.0, 0.0, alert_width-40.0, MessageLbl.frame.size.height);
            }
            
            MsgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, (MessageLbl.frame.size.height>max_msg_height)?max_msg_height:MessageLbl.frame.size.height)];
            MsgScrollView.contentSize = MessageLbl.frame.size;
            [MsgScrollView addSubview:MessageLbl];
            
            alert_height += MsgScrollView.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add buttons
        UIButton *CancelBtn;
        UIButton *OtherBtn;
        
        if (cancelButtonTitle && otherButtonTitle)
        {
            float x_displ = (int)(alert_width/2.0);
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, alert_height, x_displ, 30)];
            
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16]];
             [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            CancelBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            CancelBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ, alert_height, x_displ, 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
             [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            OtherBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                        OtherBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (cancelButtonTitle)
        {
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, alert_height, alert_width, 30)];
            
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16]];
            [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            CancelBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (otherButtonTitle)
        {
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, alert_height, alert_width, 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
            [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            OtherBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

            alert_height += OtherBtn.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add background
        
        AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0), (int)((self.frame.size.height-alert_height)/2.0), alert_width, alert_height)];
        AlertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        AlertView.backgroundColor = [UIColor whiteColor];
        AlertView.layer.cornerRadius = 5;
        
        
        [buttonBaseView setFrame:CGRectMake(0, AlertView.frame.size.height - 45, AlertView.frame.size.width, 39)];
        
        if (TitleLbl)
            [AlertView addSubview:TitleLbl];
        
        if (MsgScrollView)
            [AlertView addSubview:MsgScrollView];
        
        if (CancelBtn && OtherBtn)
        {
            [self setButtonFrame:CancelBtn];
            [self setButtonFrame:OtherBtn];
            [self manageSepraterFrame:OtherBtn];
            [buttonBaseView addSubview:CancelBtn];
            [buttonBaseView addSubview:OtherBtn];
            UIView *lineView = [[UIView alloc] init];
            [lineView setFrame:CGRectMake((AlertView.frame.size.width / 2), buttonBaseView.frame.origin.y, 1, 45)];
            [lineView setBackgroundColor:[UIColor lightGrayColor]];
            [AlertView addSubview:lineView];
            CancelBtn.clipsToBounds = true;
            OtherBtn.clipsToBounds = true;
            
            [buttonBaseView setBackgroundColor:[UIColor lightGrayColor]];
        }
        else if (CancelBtn)
        {
            [self setButtonFrame:CancelBtn];
            [buttonBaseView addSubview:CancelBtn];
            CancelBtn.clipsToBounds = true;
            
            [buttonBaseView setBackgroundColor:[UIColor lightGrayColor]];
        }
        else if (OtherBtn){
            [self setButtonFrame:OtherBtn];
            [buttonBaseView addSubview:OtherBtn];
            OtherBtn.clipsToBounds = true;
            
            [buttonBaseView setBackgroundColor:[UIColor lightGrayColor]];
        }
        [AlertView addSubview:buttonBaseView];
        buttonBaseView.clipsToBounds = true;
        [self addSubview:AlertView];
    }
    return self;
}
- (void)setButtonFrame : (UIButton*)button{
    CGRect btnOriginalFrame = button.frame;
    btnOriginalFrame.origin.y = 1;
    btnOriginalFrame.size.height = 38;
    
    [button setFrame:btnOriginalFrame];
}
- (void)manageSepraterFrame : (UIButton*)button{
    CGRect btnOriginalFrame = button.frame;
    btnOriginalFrame.origin.x += 1;
    btnOriginalFrame.size.width -= 1;
    [button setFrame:btnOriginalFrame];
}
- (id)initWithTextField:(NSString *)title message:(NSString *)message delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock2)completion
{
    self.completionBock2 = completion;
    float screenHeight;
    float screenWidth;
    
    screenHeight = window.frame.size.height;
    screenWidth = window.frame.size.width;
    
    CGRect frame;
    
    frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft || [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight)
    {
        if(!IS_OS_8_OR_LATER)
        {
            frame = CGRectMake(0.0, 0.0, screenHeight, screenWidth);
        }
    }
    
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = AlertDelegate;
        self.alpha = 1.0;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        float alert_width = 240;
        float alert_height = 5.0;
        
        //add text
        UILabel *TitleLbl;
        UIScrollView *MsgScrollView;
        
        if (title)
        {
            TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, 30.0)];
            TitleLbl.adjustsFontSizeToFitWidth = YES;
            TitleLbl.font = [UIFont fontWithName:@"GothamMedium" size:18];
            TitleLbl.textAlignment = NSTextAlignmentCenter;
            //            TitleLbl.minimumFontSize = 12.0;
            [TitleLbl setMinimumScaleFactor:12.0/[UIFont labelFontSize]];
            
            TitleLbl.backgroundColor = [UIColor clearColor];
            TitleLbl.textColor = [UIColor colorWithRed:185.0/255.0 green:150.0/255 blue:9.0/255 alpha:1.0];
            
            TitleLbl.text = title;
            
            alert_height += TitleLbl.frame.size.height + 5.0;
        }
        else
            alert_height += 15.0;
        
        if (message)
        {
            float max_msg_height = MAX_ALERT_HEIGHT - alert_height - ((cancelButtonTitle || otherButtonTitle)?(ALERT_BTN_HEIGHT+30.0):30.0);
            
            txtFieldEmail = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, alert_width-40.0, 25)];
            txtFieldEmail.textColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255 blue:94.0/255 alpha:1.0];
            txtFieldEmail.font = [UIFont fontWithName:@"GothamMedium" size:14];
            txtFieldEmail.delegate = self;
            txtFieldEmail.borderStyle = UITextBorderStyleRoundedRect;
            txtFieldEmail.frame = CGRectMake(10.0, 0.0, alert_width-40.0, txtFieldEmail.frame.size.height);
            txtFieldEmail.placeholder = @"Enter email address";
            txtFieldEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;            txtFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
            while (txtFieldEmail.frame.size.height>max_msg_height && txtFieldEmail.font.pointSize>12) {
                txtFieldEmail.font = [UIFont systemFontOfSize:txtFieldEmail.font.pointSize-1];
                [txtFieldEmail sizeToFit];
                txtFieldEmail.frame = CGRectMake(10.0, 0.0, alert_width-40.0, txtFieldEmail.frame.size.height);
            }
            
            MsgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, (txtFieldEmail.frame.size.height>max_msg_height)?max_msg_height:txtFieldEmail.frame.size.height)];
            MsgScrollView.contentSize = txtFieldEmail.frame.size;
            [MsgScrollView addSubview:txtFieldEmail];
            
            alert_height += MsgScrollView.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add buttons
        UIButton *CancelBtn;
        UIButton *OtherBtn;
        
        if (cancelButtonTitle && otherButtonTitle)
        {
            float x_displ = (int)(alert_width/2.0);
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, alert_height, x_displ, 30)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
             [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ, alert_height, x_displ, 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
            [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (cancelButtonTitle)
        {
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)(alert_width/4.0), alert_height , (alert_width/2.0), 30)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
             [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (otherButtonTitle)
        {
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)(alert_width/4.0), alert_height,(alert_width/2.0), 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
            [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += OtherBtn.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add background
        
        AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0), (int)((self.frame.size.height-alert_height)/2.0), alert_width, alert_height)];
        AlertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        AlertView.backgroundColor = [UIColor whiteColor];
        AlertView.layer.cornerRadius = 5;
        [self addSubview:AlertView];
        
        if (TitleLbl)
            [AlertView addSubview:TitleLbl];
        
        if (MsgScrollView)
            [AlertView addSubview:MsgScrollView];
        
        if (CancelBtn)
            [AlertView addSubview:CancelBtn];
        
        if (OtherBtn)
            [AlertView addSubview:OtherBtn];
    }
    return self;
}

- (id)initWithATwoTitle:(NSString *)title message:(id)message withSecondMessage:(id)secondMessage delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock1)completion
{
    self.completionBock1 = completion;
    
    float screenHeight;
    float screenWidth;
    
    screenHeight = window.frame.size.height;
    screenWidth = window.frame.size.width;
    
    CGRect frame;
    
    frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft || [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight)
    {
        if(!IS_OS_8_OR_LATER)
        {
            frame = CGRectMake(0.0, 0.0, screenHeight, screenWidth);
        }
    }
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = AlertDelegate;
        self.alpha = 1.0;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        float alert_width = 300;
        float alert_height = 5.0;
        
        //add text
        UILabel *TitleLbl;
        UIScrollView *MsgScrollView;
        
        if (title)
        {
            TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, 30.0)];
            TitleLbl.adjustsFontSizeToFitWidth = YES;
            TitleLbl.font = [UIFont fontWithName:@"GothamMedium" size:18];
            TitleLbl.textAlignment = NSTextAlignmentCenter;
            
            [TitleLbl setMinimumScaleFactor:12.0/[UIFont labelFontSize]];
            
            TitleLbl.backgroundColor = [UIColor clearColor];
            TitleLbl.textColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255 blue:94.0/255 alpha:1.0];
            TitleLbl.text = title;
            
            alert_height += TitleLbl.frame.size.height + 5.0;
        }
        else
            alert_height += 15.0;
        
        if (message)
        {
            
            float max_msg_height = MAX_ALERT_HEIGHT - alert_height - ((cancelButtonTitle || otherButtonTitle)?(ALERT_BTN_HEIGHT+30.0):30.0);
            
            UILabel *MessageLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 0.0, alert_width-30.0, 0.0)];
            MessageLbl.numberOfLines = 0;
            MessageLbl.font = [UIFont fontWithName:@"GothamMedium" size:16];
            MessageLbl.textAlignment = NSTextAlignmentCenter;
            MessageLbl.backgroundColor = [UIColor clearColor];
            MessageLbl.textColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255 blue:94.0/255 alpha:0.8];
            
            if ([message isKindOfClass:[NSMutableAttributedString class]]) {
                MessageLbl.attributedText = message;
            }
            else{
                MessageLbl.text = message;
            }
            
            [MessageLbl sizeToFit];
            MessageLbl.frame = CGRectMake(5.0, 0.0, alert_width-30.0, MessageLbl.frame.size.height);
            
            while (MessageLbl.frame.size.height>max_msg_height && MessageLbl.font.pointSize>12) {
                MessageLbl.font = [UIFont systemFontOfSize:MessageLbl.font.pointSize-1];
                [MessageLbl sizeToFit];
                MessageLbl.frame = CGRectMake(5.0, 0.0, alert_width-40.0, MessageLbl.frame.size.height);
            }
            
            MsgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, (MessageLbl.frame.size.height>max_msg_height)?max_msg_height:MessageLbl.frame.size.height)];
            MsgScrollView.contentSize = MessageLbl.frame.size;
            [MsgScrollView addSubview:MessageLbl];
            
            alert_height += MsgScrollView.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add buttons
        UIButton *CancelBtn;
        UIButton *OtherBtn;
        
        if (cancelButtonTitle && otherButtonTitle)
        {
            float x_displ = (int)((alert_width-ALERT_BTN_WIDTH*2)/3.0);
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ, alert_height, 85, 30)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
             [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ*2+ALERT_BTN_WIDTH, alert_height, 85, 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
            [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (cancelButtonTitle)
        {
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)(alert_width/4.0), alert_height , (alert_width/2.0), 30)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
             [CancelBtn setBackgroundColor:[UIColor whiteColor]];
            [CancelBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (otherButtonTitle)
        {
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)(alert_width/4.0), alert_height,(alert_width/2.0), 30)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
            [OtherBtn setBackgroundColor:[UIColor whiteColor]];
            [OtherBtn setTitleColor:alertColor forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += OtherBtn.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        UILabel *lblSecondMsg;
        
        if (secondMessage)
        {
            lblSecondMsg = [[UILabel alloc] initWithFrame:CGRectMake(15.0, alert_height + 10, alert_width-40.0, 0.0)];
            lblSecondMsg.numberOfLines = 0;
            lblSecondMsg.font = [UIFont fontWithName:@"GothamMedium" size:15];
            lblSecondMsg.textAlignment = NSTextAlignmentCenter;
            lblSecondMsg.backgroundColor = [UIColor clearColor];
            lblSecondMsg.textColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255 blue:94.0/255 alpha:0.8];
            
            if ([secondMessage isKindOfClass:[NSMutableAttributedString class]]) {
                lblSecondMsg.attributedText = secondMessage;
            }
            else{
                lblSecondMsg.text = secondMessage;
            }
            
            //                lblSecondMsg.text = secondMessage;
            
            [lblSecondMsg sizeToFit];
            lblSecondMsg.frame = CGRectMake(15.0, alert_height + 10, alert_width-40.0, lblSecondMsg.frame.size.height);
            
            alert_height += lblSecondMsg.frame.size.height + 20.0;
        }
        else
            alert_height += 15.0;
        //add background
        
        AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0), (int)((self.frame.size.height-alert_height)/2.0), alert_width, alert_height)];
        AlertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        AlertView.backgroundColor = [UIColor whiteColor];
        AlertView.layer.cornerRadius = 5;
        [self addSubview:AlertView];
        
        if (TitleLbl)
            [AlertView addSubview:TitleLbl];
        
        if (MsgScrollView)
            [AlertView addSubview:MsgScrollView];
        
        if (CancelBtn)
            [AlertView addSubview:CancelBtn];
        
        if (OtherBtn)
            [AlertView addSubview:OtherBtn];
        if (secondMessage) {
            [AlertView addSubview:lblSecondMsg];
        }
    }
    return self;
}


- (void)onBtnPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int button_index = (int)button.tag - 1000;
    
    
    
//    if (self.tag == FORGOTPASSWORD_TAG) {
//        if (self.completionBock2) {
//            self.completionBock2(self,button_index,txtFieldEmail.text);
//            self.completionBock2 = nil;
//        }
//    }
//    else{
    
        if (self.completionBock1) {
            self.completionBock1(self,button_index);
            self.completionBock1 = nil;
//        }
    }
    [self animateHide];
}

- (void)showInView:(UIView*)view
{
    if ([view isKindOfClass:[UIView class]])
    {
        [view addSubview:self];
        [self animateShow];
    }
}

- (void)animateShow
{
    AlertView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        AlertView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // do something once the animation finishes, put it here
    }];
}

- (void)animateHide
{
    AlertView.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        AlertView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
@end
