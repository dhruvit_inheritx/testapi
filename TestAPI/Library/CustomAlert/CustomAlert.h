//
//  CustomAlert.h
//  CustomAlert
//
//  Created by Mariya Kholod on 4/23/13.
//  Copyright (c) 2013 Mariya Kholod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class CustomAlert;

typedef void(^completionBock1)(CustomAlert*, NSInteger);
typedef void(^completionBock2)(CustomAlert*, NSInteger,NSString*);

@interface CustomAlert : UIView<UITextFieldDelegate>
{
    id delegate;
    UIView *AlertView;
    UITextField *txtFieldEmail;
}

@property id delegate;

@property (copy, nonatomic) void (^completionBock1)(CustomAlert*, NSInteger);
@property (copy, nonatomic) void (^completionBock2)(CustomAlert*, NSInteger,NSString*);

- (id)initWithTitle:(NSString *)title message:(id)message delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock1)completion;

- (id)initWithTextField:(NSString *)title message:(NSString *)message delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock2)completion;

- (id)initWithATwoTitle:(NSString *)title message:(id)message withSecondMessage:(id)secondMessage delegate:(id)AlertDelegate color:(UIColor*)alertColor cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle completion:(completionBock1)completion;

- (void)showInView:(UIView*)view;
- (void)animateHide;

@end

@protocol CustomAlertDelegate
- (void)customAlertView:(CustomAlert*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void)customAlertView:(CustomAlert*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex withEmail:(NSString *)strEmailID;

@end
