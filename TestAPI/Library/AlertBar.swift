//
//  AlertBar.swift
//  PerfectDemo
//
//  Created by Dhruvit on 08/05/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation
import UIKit

public enum AlertBarType {
    case Success
    case Error
    case Notice
    case Warning
    case Info
    case Custom(UIColor, UIColor)
    
    var backgroundColor: UIColor {
        get {
            switch self {
            case .Success:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0x4CAF50)
            case .Error:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0xf44336)
            case .Notice:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0x2196F3)
            case .Warning:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0xFFC107)
            case .Info:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0x009688)
            case .Custom(let backgroundColor, _):
                return backgroundColor
            }
        }
    }
    var textColor: UIColor {
        get {
            switch self {
            case .Custom(_, let textColor):
                return textColor
            default:
                return AlertBarHelper.UIColorFromRGB(rgbValue: 0xFFFFFF)
            }
        }
    }
}

public class AlertBar: UIView {
    
    public static var textAlignment: NSTextAlignment = .left
    static var alertBars: [AlertBar] = []
    
    let messageLabel = UILabel()
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        messageLabel.frame = CGRect(x: 2, y: 2, width: frame.width - 4, height: frame.height - 4)
        messageLabel.numberOfLines = 0
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.addSubview(messageLabel)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleRotate(notification:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    @objc dynamic private func handleRotate(notification: NSNotification)
    {
        self.removeFromSuperview()
        AlertBar.alertBars = []
    }
    
    public class func show(type: AlertBarType, message: String,parentView : UIView , duration: Double = 2, completion: (() -> Void)? = nil) {
        //        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.height
        let statusBarHeight : CGFloat = 64.0
        
        //        let KSCREENWIDTH = UIScreen.main.bounds.width
        //        let KSCREENHEIGHT = UIScreen.main.bounds.height
        
        let alertBar = AlertBar(frame: CGRect(x: 0, y: 0, width: KSCREENWIDTH, height: statusBarHeight))
        alertBar.messageLabel.text = message
        //        alertBar.messageLabel.textAlignment = AlertBar.textAlignment
        alertBar.messageLabel.textAlignment = .center
        alertBar.backgroundColor = type.backgroundColor
        alertBar.messageLabel.textColor = type.textColor
        AlertBar.alertBars.append(alertBar)
        
        //        let width = UIScreen.main.bounds.width
        //        let height = UIScreen.main.bounds.height
        
        let baseView = UIView(frame: UIScreen.main.bounds)
        baseView.isUserInteractionEnabled = false
        baseView.addSubview(alertBar)
        
        //        let window: UIWindow
        //        let orientation = UIApplication.shared.statusBarOrientation
        //        if orientation.isLandscape {
        //            window = UIWindow(frame: CGRect(x: 0, y: 0, width: height, height: width))
        //            let sign: CGFloat = orientation == .landscapeLeft ? -1 : 1
        //            let d = fabs(width - height) / 2
        //            baseView.transform = CGAffineTransform(rotationAngle: sign * CGFloat(Double.pi) / 2).translatedBy(x: sign * d, y: sign * d)
        //        } else {
        //            window = UIWindow(frame: CGRect(x: 0, y: 0, width: width, height: height))
        //            if orientation == .portraitUpsideDown {
        //                baseView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        //            }
        //        }
        //        window.isUserInteractionEnabled = false
        //        window.windowLevel = UIWindowLevelStatusBar + 1 + CGFloat(AlertBar.alertBars.count)
        //        window.addSubview(baseView)
        //        window.makeKeyAndVisible()
        
        parentView.addSubview(baseView)
        
        alertBar.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
        UIView.animate(withDuration: 0.2,
                       animations: { () -> Void in
                        alertBar.transform = CGAffineTransform.identity
        }, completion: { _ in
            UIView.animate(withDuration: 0.2,
                           delay: duration,
                           options: .curveEaseInOut,
                           animations: { () -> Void in
                            alertBar.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
            },
                           completion: { (animated: Bool) -> Void in
                            alertBar.removeFromSuperview()
                            if let index = AlertBar.alertBars.index(of: alertBar) {
                                
                                UIApplication.shared.isStatusBarHidden = false
                                UIApplication.shared.statusBarStyle = .lightContent
                                AlertBar.alertBars.remove(at: index)
                            }
                            // To hold window instance
                            //                            window.isHidden = true
                            completion?()
            })
        })
    }
    
    public class func showError(error: NSError,parentView : UIView ,duration: Double = 2, completion: (() -> Void)? = nil) {
        let code = error.code
        let localizedDescription = error.localizedDescription
        self.show(type: .Error, message: "(\(code)) " + localizedDescription, parentView: parentView)
    }
}

internal class AlertBarHelper {
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
