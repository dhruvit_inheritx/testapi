//
//  Constants.swift
//

import Foundation
import UIKit

// MARK: - iPhone Model

let IS_IPHONE_4_OR_LESS = UIScreen .main.bounds.size.height < 568.0
let IS_IPHONE_5 = UIScreen .main.bounds.size.height == 568.0
let IS_IPHONE_6 = UIScreen .main.bounds.size.height == 667.0
let IS_IPHONE_6P = UIScreen .main.bounds.size.height == 736.0
let IS_OS_8_OR_LATER = (UIDevice.current.systemVersion as NSString).floatValue >= 8.0

// MARK: - Application

let BUNDLESHORTVERSION = "CFBundleShortVersionString"

let KSCREENWIDTH = UIScreen.main.bounds.width
let KSCREENHEIGHT = UIScreen.main.bounds.height

let APPDELEGATE:AppDelegate =  UIApplication.shared.delegate as! AppDelegate
let cApplication = "TestAPI"

let BUILD = "1"

// UserDefault
let USERDEFAULTS = UserDefaults.standard

// NotificationCenter
let NOTIFICATIONCENTER = NotificationCenter.default

//API Spec

let APIKEY = "ba1f0ea830c460cc23c8c195196ced76"
let APISECRET = "secret"

let KBASEURL = "http://inheritxdev.net/PerfectAttire_dev/code/web/api/v1/api/"

// MARK: - Constants StoryBoard

let sMain = "Main"

// MARK: - Bool

let kISUSERLOGGEDIN = "isLogged"

// Color

let kThemeMainLabelColor = UIColor(red: 120/255, green: 187/255, blue: 106/255, alpha: 1)
let kThemeDestructiveColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
let kThemeColor = UIColor(red: 120/255, green: 187/255, blue: 106/255, alpha: 1)

// Message
let APPNAME = "TestAPI"

// MARK: - Parameters

let KUSERDETAILS = "UserDetails"
let KUSERDETAILS_RESPONSE = "userDetails"

let EMPTYSTRING = ""
let KDEVICETOKEN = "device_token"
let KID = "id"
let KFBID = "fb_id"
let KEMAIL = "email"
let KFBFIRSTNAME = "first_name"
let KFBLASTNAME = "last_name"
let KGENDER = "gender"
let KCONTACTNO = "contact_no"
let KFBPICTURE = "picture"
let KFBDATA = "data"
let KFBURL = "url"
let KPROFILEPICTURE = "profile_pic"
let KACCESSTOKEN = "access_token"
let KDOB = "dob"
let KLATITUDE = "latitude"
let KLONGITUDE = "longitude"

let KERROR = "error"
let KMESSAGE = "message"
let KDATA = "data"

// Button title
let BTNOKTITLE = "OK"
let BTNRESETTITLE = "Reset"
let BTNREQUESTTITLE = "Request"
let BTNCANCELTITLE = "Cancel"
let BTNYESTITLE = "Yes"
let BTNNOTITLE = "No"


let STREMPTY = ""
let EMAILREGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let PHONECHARS = "+0123456789"

let  CGFLOATZERO : CGFloat = 0.0
let FLOATZERO : Float = 0.0
let INTZERO : Int = 0
