//
//  Dictionary+Common.swift
//

import Foundation
import UIKit

//extension Dictionary where Value: AnyObject {
//    
//    var nullsRemoved: [Key: Value] {
//        let tup = filter { !($0.1 is NSNull) }
//        return tup.reduce([Key: Value]()) { (var r, e) in r[e.0] = e.1; return r }
//    }
//}

extension Dictionary {
    
    func getAllValues() -> [Any] {
        return self.flatMap(){ $0.1 }
    }
    
    /// An immutable version of update. Returns a new dictionary containing self's values and the key/value passed in.
    func updatedValue(_ value: Value, forKey key: Key) -> Dictionary<Key, Value> {
        var result = self
        result[key] = value
        return result
    }
    
    var nullsRemoved: [Key: Value] {
        
        let tup = filter { !($0.1 is NSNull) }
//        return tup.reduce([Key: Value]()) { $0.0.updatedValue($0.1.value, forKey: $0.1.key) }
        return tup
    }
    
    func getValueIfAvilable(key: Key) -> Value? {
        // if key not found, replace the nil with
        // the first element of the values collection
        if let val = self[key] {
            return val
        }
        return nil
        // note, this is still an optional (because the
        // dictionary could be empty)
    }
}
